create database myshop;

use myshop;

create table USER(
	ID int auto_increment,
    USER_CODE nvarchar(50) not null,
    USERNAME nvarchar(50) ,
    PASSWORD nvarchar(255),
    FULL_NAME nvarchar(255),
    AGE int,
    GENDER int,
    PHONE_NUMBER nvarchar(10),
    ADDRESS nvarchar(255),
    EMAIL nvarchar(255) not null,
    STATUS int,
    constraint primary key(ID)
);
alter table USER
add LOGIN_FAILED int;

create table ROLE(
	ID int auto_increment,
    ROLE_NAME nvarchar(20),
    constraint primary key(ID)
);

create table USER_ROLE(
	ID int auto_increment,
    USER_ID int not null,
    ROLE_ID int not null,
    constraint primary key(ID)
);

create table CATEGORY(
	ID int auto_increment,
    CATEGORY_NAME nvarchar(255),
    constraint primary key(ID)
);

create table PRODUCT(
	ID int auto_increment,	
    CATEGORY_ID int,
    PRODUCT_NAME nvarchar(255),
    constraint primary key(ID)
);

create table SIZE(
	ID int primary key auto_increment,	
    SIZE_NAME nvarchar(50) not null
);
alter table SIZE
drop column SIZE_NAME,
add SCREEN_SIZE Double;

create table COLOR(
	ID int primary key auto_increment,	
    COLOR_NAME nvarchar(50) not null
);

create table PRODUCT_DETAIL(
	ID int primary key auto_increment,	
    PRODUCT_ID int,
    SIZE_ID int,
    COLOR_ID int,
    AMOUNT int,
    SINGLE_PRICE Double,
    DATETIME_IMPORTED timestamp(0),
    STATUS int
);
alter table PRODUCT_DETAIL
add DESCRIPTION nvarchar(255),
add IMAGE nvarchar(1000);

create table CART(
	ID int primary key auto_increment,	
    PRODUCT_DETAIL_ID int,
    AMOUNT int,
    DATETIME_EXPIRED timestamp(0)
);
alter table CART
add USER_ID int;

create table BILL(
	ID int primary key auto_increment,	
    USER_ID int,
    TOTAL_MONEY float,
    DATETIME_BOUGHT timestamp(0),
    PAYMENT int
);

create table PRODUCT_DETAIL_BILL(
	ID int primary key auto_increment,	
    PRODUCT_DETAIL_ID int,
    BILL_ID int,
    AMOUNT int
);

create table TRANSPORTATION(
	ID int primary key auto_increment,	
    BILL_ID int,
    STATUS_BILL int
);

create table PRODUCT_DETAIL_COMMENT(
	ID int primary key auto_increment,	
    USER_ID int,
    RATE int,
    COMMENT nvarchar(255)
);
alter table PRODUCT_DETAIL_COMMENT
add PRODUCT_DETAIL_ID int;

create table SALE_OFF(
	ID int primary key auto_increment,	
    SALE_OF_NAME nvarchar(255),
    START_TIME timestamp(0),
    END_TIME timestamp(0)
);
	
create table SALE_OFF_DETAIL(
	ID int primary key auto_increment,
	PRODUCT_DETAIL_ID int,
    SALE_OFF_ID int,
    PERCENT_REDUCING float
);

create table CONTACT(
	ID int primary key auto_increment,
    NAME nvarchar(50),
	PHONE int,
    COMMENT nvarchar(255)
);

create table CHAT(
	ID int auto_increment,
    CREATED_USER int,
    DATETIME_CREATED timestamp,
    CHAT_NAME nvarchar(225),
    constraint primary key(ID)
);

create table MESSAGE(
	ID int auto_increment,
    SENDER_ID int not null,
    RECEIVER_ID int,
    CHAT_ID int,
    TEXT nvarchar(2000),
    DATETIME_SENT timestamp,
    STATUS int,
    constraint primary key(ID)
);

create table USER_CHAT (
	ID int auto_increment,
    USER_ID int,
    CHAT_ID int,
    constraint primary key(ID)
);

insert into USER(USER_CODE, USERNAME, PASSWORD, FULL_NAME, AGE, GENDER, PHONE_NUMBER, EMAIL, ADDRESS, STATUS)
values('usercode1', 'username1', 'password1', 'fullname1', 20, 0, '1234567890', 'abc1@gmail.com' ,'Hà nội', 1);
insert into USER(USER_CODE, USERNAME, PASSWORD, FULL_NAME, AGE, GENDER, PHONE_NUMBER, EMAIL, ADDRESS, STATUS)
values('usercode2', 'username2', 'password2', 'fullname2', 20, 0, '1234567890', 'abc2@gmail.com' ,'Hà nội', 1);
insert into USER(USER_CODE, USERNAME, PASSWORD, FULL_NAME, AGE, GENDER, PHONE_NUMBER, EMAIL, ADDRESS, STATUS)
values('usercode3', 'username3', 'password3', 'fullname3', 20, 0, '1234567890', 'abc3@gmail.com' ,'Hà nội', 1);
insert into USER(USER_CODE, USERNAME, PASSWORD, FULL_NAME, AGE, GENDER, PHONE_NUMBER, EMAIL, ADDRESS, STATUS)
values('usercode4', 'username4', 'password4', 'fullname4', 20, 0, '1234567890', 'abc4@gmail.com' ,'Hà nội', 1);
insert into USER(USER_CODE, USERNAME, PASSWORD, FULL_NAME, AGE, GENDER, PHONE_NUMBER, EMAIL, ADDRESS, STATUS)
values('usercode5', 'username5', 'password5', 'fullname5', 20, 0, '1234567890', 'abc5@gmail.com' ,'Hà nội', 1);


insert into ROLE(ROLE_NAME) values('ROLE_ADMIN');
insert into ROLE(ROLE_NAME) values('ROLE_EMPLOYEE');
insert into ROLE(ROLE_NAME) values('ROLE_USER');


insert into USER_ROLE(USER_ID, ROLE_ID) values(1, 1);
insert into USER_ROLE(USER_ID, ROLE_ID) values(2, 2);
insert into USER_ROLE(USER_ID, ROLE_ID) values(3, 2);
insert into USER_ROLE(USER_ID, ROLE_ID) values(4, 3);
insert into USER_ROLE(USER_ID, ROLE_ID) values(5, 3);


insert into CATEGORY(CATEGORY_NAME) values("DELL");
insert into CATEGORY(CATEGORY_NAME) values("ASUS");
insert into CATEGORY(CATEGORY_NAME) values("MACBOOK");
insert into CATEGORY(CATEGORY_NAME) values("LENOVO");


insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell Inspiron");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell Vostro");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell XPS");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell Alienware");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell Precision");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(1, "Dell Latitude");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(2, "Asus ZenBook");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(2, "Asus VivoBook");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(2, "Asus ROG Strix");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(2, "Asus Chromebook Flip");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(3, "Macbook Pro M1");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(3, "Macbook Pro M2");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(3, "Macbook Pro Touch Bar");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(3, "Macbook Air M1 ");
insert into PRODUCT(CATEGORY_ID, PRODUCT_NAME) values(3, "Macbook Air M2 ");

insert into SIZE(SCREEN_SIZE) values(11.5);
insert into SIZE(SCREEN_SIZE) values(13);
insert into SIZE(SCREEN_SIZE) values(14);
insert into SIZE(SCREEN_SIZE) values(15);
insert into SIZE(SCREEN_SIZE) values(15.5);

insert into COLOR(COLOR_NAME) values("Đen");
insert into COLOR(COLOR_NAME) values("Bạc");
insert into COLOR(COLOR_NAME) values("Xám");
insert into COLOR(COLOR_NAME) values("Hồng");

insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 4, 1, 100, 15000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 5, 1, 200, 17000000, '2021-12-24 11:10:21');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 4, 2, 150, 15000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 4, 3, 100, 15000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 4, 4, 120, 15000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(2, 3, 1, 100, 18000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(2, 3, 2, 100, 16000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(2, 5, 3, 100, 12000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(2, 4, 3, 100, 15000000, '2021-12-24 11:10:20');
insert into PRODUCT_DETAIL(PRODUCT_ID, SIZE_ID, COLOR_ID, AMOUNT, SINGLE_PRICE, DATETIME_IMPORTED) 
values(1, 4, 1, 100, 15500000, '2021-12-24 11:10:20');


insert into CART(PRODUCT_DETAIL_ID, AMOUNT, DATETIME_EXPIRED, USER_ID) values(1, 1, '2021-12-25 11:10:20', 4);
insert into CART(PRODUCT_DETAIL_ID, AMOUNT, DATETIME_EXPIRED, USER_ID) values(2, 2, '2021-12-25 11:10:20', 5);
insert into CART(PRODUCT_DETAIL_ID, AMOUNT, DATETIME_EXPIRED, USER_ID) values(3, 1, '2021-12-25 11:10:20', 4);

insert into BILL(USER_ID, TOTAL_MONEY, DATETIME_BOUGHT, PAYMENT) values(4, 20000000, '2021-12-25 11:10:20', 0);  -- id = 1

insert into PRODUCT_DETAIL_BILL(PRODUCT_DETAIL_ID, BILL_ID, AMOUNT) values(1, 1, 1);
insert into PRODUCT_DETAIL_BILL(PRODUCT_DETAIL_ID, BILL_ID, AMOUNT) values(2, 1, 2);
insert into PRODUCT_DETAIL_BILL(PRODUCT_DETAIL_ID, BILL_ID, AMOUNT) values(3, 1, 1);

insert into TRANSPORTATION(BILL_ID, STATUS_BILL) values (1, 0);

insert into PRODUCT_DETAIL_COMMENT(USER_ID, RATE, COMMENT, PRODUCT_DETAIL_ID) values(4, 4, 'Tạm ổn', 1);
insert into PRODUCT_DETAIL_COMMENT(USER_ID, RATE, COMMENT, PRODUCT_DETAIL_ID) values(5, 1, 'Sản phẩm như loz', 2);

insert into SALE_OFF(SALE_OF_NAME, START_TIME, END_TIME) 
values('Khuyến mại mùa tết', '2021-12-25 11:10:20', '2021-12-26 11:10:20');

insert into SALE_OFF_DETAIL(PRODUCT_DETAIL_ID, SALE_OFF_ID, PERCENT_REDUCING) values(1, 1, 20);
insert into SALE_OFF_DETAIL(PRODUCT_DETAIL_ID, SALE_OFF_ID, PERCENT_REDUCING) values(2, 1, 15);
insert into SALE_OFF_DETAIL(PRODUCT_DETAIL_ID, SALE_OFF_ID, PERCENT_REDUCING) values(3, 1, 10);
insert into SALE_OFF_DETAIL(PRODUCT_DETAIL_ID, SALE_OFF_ID, PERCENT_REDUCING) values(4, 1, 20);

insert into CONTACT(NAME, PHONE, COMMENT) values('Nguyễn Văn A', 1234567890, 'Shop như cak');
insert into CONTACT(NAME, PHONE, COMMENT) values('Nguyễn Văn B', 1234567890, 'Shop như cak 2 ');
insert into CONTACT(NAME, PHONE, COMMENT) values('Nguyễn Văn C', 1234567890, 'Shop như cak 3');

insert into CHAT(CREATED_USER, DATETIME_CREATED, CHAT_NAME) values(1, '2022-09-23', 'Chatroom 01');
insert into CHAT(CREATED_USER, DATETIME_CREATED, CHAT_NAME) values(2, '2022-09-23', 'Chatroom 02');


