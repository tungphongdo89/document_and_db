use MyShop;

select * from user;
select * from user_role;

select * from PRODUCT;
select * from PRODUCT_DETAIL;


/* Create procedure get product by ID*/
DELIMITER $$
DROP PROCEDURE IF EXISTS getProductById $$
CREATE PROCEDURE getProductById(IN productId INT)
BEGIN
	SELECT * FROM product WHERE ID = productId;
END; $$

/* Query for Chatting */
-- select list chats which include you
/*
	select ID, CREATED_USER, DATETIME_CREATED, CHAT_NAME
	from CHAT
	where ID in (
			select CHAT_ID 
			from USER_CHAT 
			where USER_ID = 2
		);
*/
    
select t.ID, t.CREATED_USER, t.DATETIME_CREATED, t.CHAT_NAME
from CHAT t
inner join USER_CHAT uc
on t.ID = uc.CHAT_ID
where uc.USER_ID = 2;

-- select list users whom you sent message

select distinct u.ID, u.FULL_NAME
from USER u
inner join MESSAGE m
on u.ID = m.RECEIVER_ID
where m.RECEIVER_ID is not null
and m.SENDER_ID = 1
order by u.ID desc;

-- create index for table
create index idx_product_id on PRODUCT_DETAIL(PRODUCT_ID);
show indexes from PRODUCT_DETAIL
select * from PRODUCT_DETAIL;

create index idx_screen_size on SIZE(SCREEN_SIZE);
show indexes from SIZE;
select * from SIZE;

create index idx_color_name on COLOR(COLOR_NAME);
show indexes from COLOR;


-- for get-all-product
explain select 
   pd.ID id,
   pd.PRODUCT_ID productId,
   p.PRODUCT_NAME productName,
   cg.ID categoryId,
   cg.CATEGORY_NAME categoryName,
   sz.ID sizeId,
   sz.SCREEN_SIZE screenSize,
   cl.ID colorId,
   cl.COLOR_NAME colorName,
   pd.AMOUNT amount,
   pd.SINGLE_PRICE singlePrice,
   convert( pd.DATETIME_IMPORTED, char) datetimeImported,
   pd.DESCRIPTION description,
   pd.IMAGE image
from PRODUCT_DETAIL pd
   join PRODUCT p on pd.PRODUCT_ID = p.ID
   join CATEGORY cg on p.CATEGORY_ID = cg.ID
   join SIZE sz on pd.SIZE_ID = sz.ID
   join COLOR cl on pd.COLOR_ID = cl.ID
where 1=1
and cg.ID = 1
and pd.PRODUCT_ID = 1
and sz.SCREEN_SIZE = 15
and lower(cl.COLOR_NAME) like lower('%Đen%');


drop index idx_product_id on PRODUCT_DETAIL
ALGORITHM = INPLACE 
LOCK = DEFAULT;

drop index idx_screen_size on SIZE
ALGORITHM = INPLACE 
LOCK = DEFAULT;

drop index idx_color_name on COLOR
ALGORITHM = INPLACE 
LOCK = DEFAULT;










